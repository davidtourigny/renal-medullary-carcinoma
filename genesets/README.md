# Gene sets

This directory contains the hallmark  geneset file and ferroptosis gene set *WP_FERROPTOSIS* downloaded from [MSigDB]( https://www.gsea-msigdb.org/gsea/msigdb/genesets.jsp?collection=H), and a script that generates gene set files (in GMT format) from the marker files from the supplementary of this [paper](https://science.sciencemag.org/content/365/6460/1461/). 

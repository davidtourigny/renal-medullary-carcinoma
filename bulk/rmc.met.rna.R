library(dplyr)
library(DESeq2)
library(GSVA)
library(GSEABase)
library(matrixTests)
library(ggplot2)
library(ggpubr)
library(gplots)
library(matrixTests)
library(psycho)

maxn <- function(n) function(x) order(x, decreasing = TRUE)[n]

# Function for PCA plot
get.pca <- function (data.in, title) {
  
  data <- data.in
  data$TN <- NULL
  data <- na.omit(data)
  data <- scale(data)
  data.cov <- cov(data)
  data.eigen <- eigen(data.cov)
  phi <- data.eigen$vectors[,1:10]
  print("PC1 explains ")
  print(data.eigen$values[1]/sum(data.eigen$values))
  print("PC2 explains ")
  print(data.eigen$values[2]/sum(data.eigen$values))
  phi <- -phi
  row.names(phi) <- colnames(data)
  colnames(phi) <- paste("PC",c(1:10),sep="")
  PC <- as.matrix(data) %*% phi
  PC <- data.frame(PC)
  PC$TN <- data.in$TN
  plot <- ggscatter(
    PC, x = "PC1", y = "PC2",
    xlab = "First Principal Component",
    ylab = "Second Principal Component",
    color = "TN",
    palette = c("black", "red"),
    title = title,
    show.legend.text = F,
    label = rownames(data.in),
    label.select = c("RC20.MSKC.00627","RC20.MSKC.00628")
  )
  result <- list(plot = plot, u1 = phi[,1], u2 = phi[,2], PC = PC)
  
  # Get largest metaboite counts for RMC Tumor
  data <- t(data)
  max.scores <- colnames(data)[apply(data, 1, maxn(5))]
  names(max.scores) <- rownames(data)
  rmc.max.scores <- names(max.scores)[which(max.scores == "RC20.MSKC.00627")]
  print("Max scores for RMC")
  print(rmc.max.scores)
  return(result)
  
}

# Create mapping info
mapping <- read.csv("MasterMappingRCC.csv", header = TRUE, row.names=1)
mapping$RNAID <- sub("-",".",mapping$RNAID)
mapping$MetabID <- sub("-",".",mapping$MetabID)

# Read and format raw RNA-seq data to matrix 
rna.1 <- read.csv("rna-data/MultiRegionalRCC.raw.csv", header = TRUE, row.names=1)
rna.2 <- read.csv("rna-data/Project_08402_M.raw.csv", header = TRUE, row.names=1)
rna.3 <- read.csv("rna-data/Multiregions.2Batchs.Sample.hg19KnownGene.raw.csv", header = TRUE, row.names=1)
rna.4 <- read.csv("rna-data/Flow4Batch.raw.csv", header = TRUE, row.names=1)
rna <- merge(rna.1, rna.2, by=0, all=TRUE)
rownames(rna) <- rna$Row.names
rna$Row.names <- NULL
rna.3 <- rna.3[, -which(colnames(rna.3) %in% colnames(rna))] # duplicates
rna <- merge(rna, rna.3, by=0, all=TRUE)
rownames(rna) <- rna$Row.names
rna$Row.names <- NULL
rna <- merge(rna, rna.4, by=0, all=TRUE)
rownames(rna) <- rna$Row.names
rna$Row.names <- NULL
rm(rna.1,rna.2,rna.3,rna.4)
rna <- rna[,which(colnames(rna) %in% mapping$RNAID)]
rna <- rna[,mapping$RNAID]
colnames(rna) <- rownames(mapping)
rna.counts <- na.omit(rna)

# Read and subset metabolomics data
met.1 <- read.csv("met-data/RC18_MetabImmune.csv", header = TRUE, row.names=1)
met.1.annotations <- read.csv("met-data/RC18_annotations.csv", header = TRUE, row.names=1)
met.1 <- t(scale(t(met.1)))
met.2 <- read.csv("met-data/RC20_MetabImmune.csv", header = TRUE, row.names=1)
met.2.annotations <- read.csv("met-data/RC20_annotations.csv", header = TRUE, row.names=1)
met.2 <- t(scale(t(met.2)))
met <- merge(met.1, met.2, by=0, all=TRUE)
rownames(met) <- met$Row.names
met$Row.names <- NULL
rm(met.1,met.2)
met <- met[,which(colnames(met) %in% mapping$MetabID)]
mapping <- mapping[which(mapping$MetabID %in% colnames(met)), ]
met <- met[,mapping$MetabID]
colnames(met) <- gsub("-",".",gsub(":",".",rownames(mapping)))
met <- na.omit(met)

# PCA for metabolite data
met.data <- data.frame(t(met))
met.data$TN <- mapping$TN
met.pca <- get.pca(met.data, "PCA of metabolite data")
met.pca$plot 

# Crawford-Howell test for RMC metabolites over tumor samples only
# Extract those with significant BF corrected p-value 
met.tumor <- met.data[which(met.data$TN == "Tumor"),]
met.tumor$TN <- NULL
met.tumor <- t(met.tumor)
crawford.howell <- function(x) {
  crawford.test(x["RC20.MSKC.00627"], x[names(x) != "RC20.MSKC.00627"]) 
}
result <- apply(met.tumor, 1, crawford.howell)
extract.p <- function(x) {
  x$summary$p
}
result.summary <- sapply(result, extract.p)
result.signif <- result.summary[which(result.summary < 0.05/nrow(met.tumor))]
names(result.signif)

# Crawford-Howell test for RMC metabolites over normal samples only
# Extract those with significant BF corrected p-value 
met.normal <- met.data[which(met.data$TN == "Normal"),]
met.normal$TN <- NULL
met.normal <- t(met.normal)
crawford.howell <- function(x) {
  crawford.test(x["RC20.MSKC.00628"], x[names(x) != "RC20.MSKC.00628"]) 
}
result <- apply(met.normal, 1, crawford.howell)
extract.p <- function(x) {
  x$summary$p
}
result.summary <- sapply(result, extract.p)
result.signif <- result.summary[which(result.summary < 0.05/nrow(met.normal))]
names(result.signif)

# Subset RNA-seq data to those for which metabolites available
# Convert to SummarizedExperiment and pre-process using DESeq2
rna.counts <- rna.counts[rownames(mapping)]
rna.counts <- as.matrix(rna.counts)
col.data <- data.frame(TN = mapping$TN)
col.data$TN <- factor(col.data$TN)
rna.dss <- DESeqDataSetFromMatrix(countData = rna.counts, colData = col.data, design = ~TN)
rna.dss <- DESeq(rna.dss)
rna.dss.vst <- vst(rna.dss)
rna.norm <- assay(rna.dss.vst)
rna.res <- results(rna.dss, contrast=c("TN", "Tumor", "Normal"))

#Perform GSVA using gene sets
gene_sets <- getGmt("gene_sets/ferroptosis.gmt")
genes <- unique(unlist(geneIds(gene_sets)))
gene.results <- rna.norm[which(rownames(rna.norm) %in% genes),]
colnames(gene.results) <- gsub("-",".",gsub(":",".",colnames(gene.results)))
pathway.dge <- na.omit(rna.res[genes,])
colnames(pathway.dge) <- c("baseMean", "avg_logFC", "lfcSE", "stat", "p_val", "p_val_adj")
gsva.analysis <- gsva(rna.norm, gene_sets, method = "gsva", min.sz = 5, max.sz=Inf, verbose=TRUE)
gsva.results <- data.frame(gsva.analysis)
gsva.tumor <- gsva.results[,which(col.data$TN == "Tumor")]
gsva.normal <- gsva.results[,which(col.data$TN == "Normal")]
crawford.howell <- function(x) {
  crawford.test(x["RC20.MSKC.00628"], x[names(x) != "RC20.MSKC.00628"]) 
}
result <- apply(gsva.normal, 1, crawford.howell)

# Get largest GSVA scores for RMC Tumor
max.scores <- colnames(gsva.results)[apply(gsva.results,1,which.max)]
names(max.scores) <- rownames(gsva.results)
rmc.max.scores <- names(max.scores)[which(max.scores == "RC20.MSKC.00627")]
rmc.max.scores





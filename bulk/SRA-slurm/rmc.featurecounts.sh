#!/bin/sh
#
#SBATCH --account=iicd
#SBATCH --job-name=ALIGNRMC
#SBATCH --ntasks-per-node=16
#SBATCH --time=23:00:00
#SBATCH --mem-per-cpu=10gb
# assumes that subread installed in conda environment using conda install -c bioconda subread

module load anaconda/3-5.3.1
source activate hisat2
featureCounts -p -t exon -g gene_id -a /moto/iicd/users/dst2156/hg19.ncbiRefSeq.gtf -o counts.paired.txt /moto/iicd/users/dst2156/*.bam
conda deactivate



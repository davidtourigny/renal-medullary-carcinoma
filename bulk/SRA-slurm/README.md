# HPC scripts for generating counts matrix from SRA data

The scripts depend on several tools and additional files:

* [SRA Toolkit](https://hpc.nih.gov/apps/sratoolkit.html) binary in appropriate location (e.g. user's home directory, modify [rmc.fastq.sh](./rmc.fastq.sh) appropriately)
* *OPTIONAL:* [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) binary in appropriate location (e.g. user's home directory, modify [rmc.fastqc.sh](./rmc.fastqc.sh) appropriately)

* Python3 conda environment with [hisat2](https://anaconda.org/bioconda/hisat2), [samtools](https://anaconda.org/bioconda/samtools) and [subread](https://anaconda.org/bioconda/subread) installed

* Copies of the hg19 UCSC genome index available [here](http://daehwankimlab.github.io/hisat2/download/#h-sapiens) and a GTF of the transcript set available [here](https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/genes/)   

The directives in these scripts are to be adaped to the user's HPC scheduling architecture (in this case SLURM). The procedure for generating the counts matrix from SRA data [PRJNA605003](https://www.ncbi.nlm.nih.gov/sra/?term=PRJNA605003)  is as follows:

* Raw SRA files can be downloaded and converted to fastq format using the script [rmc.fastq.sh](./rmc.fastq.sh) 

* *OPTIONAL:* quailty metrics of fastq reads can be generated using the script [rmc.fastqc.sh](./rmc.fastqc.sh) 

* Paired reads from fastq files are mapped and aligned to the reference genome index using the script [rmc.hisat2-run.sh](./rmc.hisat2-run.sh) 

* Reads are used to estimate transcript counts using the script [rmc.featurecounts.sh](./rmc.featurecounts.sh) 

The final output of the pipeline will include a file ``counts.paired.txt`` that can be used for downstream gene expression analysis.

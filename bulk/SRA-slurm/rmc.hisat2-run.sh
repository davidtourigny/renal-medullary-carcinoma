#!/bin/sh
#
#SBATCH --account=iicd
#SBATCH --job-name=ALIGNRMC
#SBATCH --ntasks-per-node=16
#SBATCH --time=23:00:00
#SBATCH --mem-per-cpu=10gb
# assumes that hisat2 and samtools installed in conda environment using conda install -c bioconda hisat2 samtools

module load anaconda/3-5.3.1
source activate hisat2

for i in $(seq 595 611)
do
	hisat2 -p 8 --dta -x /moto/iicd/users/dst2156/hg19/genome -1 SRR11028$i"_1.fastq" -2 SRR11028$i"_2.fastq" -S SRR11028$i.sam 
	samtools sort -@ 8 -o SRR11028$i.bam SRR11028$i.sam
done
conda deactivate



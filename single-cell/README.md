# Processing and analysis of single-cell RNA-seq data from 10x

This directory contains scripts for processing and analysis of single-cell RNA-seq data on an RMC case reported in [paper](link). 

The file [functions](./functions.R) contains some custom R functions for single-cell analysis.

The script [process.rmc.from10x](./process.rmc.from10x.R) uses [Seurat](https://satijalab.org/seurat/) to quality control and assign preliminary cell types from 10x data supplied in a directory ``filtered_feature_bc_matrix`` (not provided). The script outputs some plots associated with processing and analysis, and the normalised counts table and metadata associated with [paper](link).


